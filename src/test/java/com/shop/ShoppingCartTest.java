package com.shop;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.math.BigDecimal;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ShoppingCartTest {

    @Test
    public void testAppleCost60p() {

        ShoppingCart shoppingCart = new ShoppingCart();

        BigDecimal totalPrice = shoppingCart.checkout("apple");

        assertThat(totalPrice, equalTo(new BigDecimal("0.60")));
    }

    @Test
    public void testOrangeCost25p() {

        ShoppingCart shoppingCart = new ShoppingCart();

        BigDecimal totalPrice = shoppingCart.checkout("orange");

        assertThat(totalPrice, equalTo(new BigDecimal("0.25")));
    }

    @Test
    public void testCheckoutAddsUpCostOfItems() {

        ShoppingCart shoppingCart = new ShoppingCart();

        BigDecimal totalPrice = shoppingCart.checkout("apple", "orange");

        assertThat(totalPrice, equalTo(new BigDecimal("0.85")));
    }

    @Test(dataProvider = "buyOneAppleGetOneFreeDataProvider")
    public void testBuyOneAppleGetOneFree(String[] items, BigDecimal expectedPrice) {

        ShoppingCart shoppingCart = new ShoppingCart();

        BigDecimal totalPrice = shoppingCart.checkout(items);

        assertThat(totalPrice, equalTo(expectedPrice));
    }

    @DataProvider
    public Object[][] buyOneAppleGetOneFreeDataProvider() {
        return new Object[][] {
            {new String[] {"apple", "apple"},                   new BigDecimal("0.60")},
            {new String[] {"apple", "apple", "apple"},          new BigDecimal("1.20")},
            {new String[] {"apple", "apple", "apple", "apple"}, new BigDecimal("1.20")}
        };
    }

    @Test(dataProvider = "threeOrangeForThePriceOfTwoDataProvider")
    public void testThreeOrangeForThePriceOfTwo(String[] items, BigDecimal expectedPrice) {

        ShoppingCart shoppingCart = new ShoppingCart();

        BigDecimal totalPrice = shoppingCart.checkout(items);

        assertThat(totalPrice, equalTo(expectedPrice));
    }

    @DataProvider
    public Object[][] threeOrangeForThePriceOfTwoDataProvider() {
        return new Object[][] {
            {new String[] {"orange", "orange"},                     new BigDecimal("0.50")},
            {new String[] {"orange", "orange", "orange"},           new BigDecimal("0.50")},
            {new String[] {"orange", "orange", "orange", "orange"}, new BigDecimal("0.75")}
        };
    }
}
