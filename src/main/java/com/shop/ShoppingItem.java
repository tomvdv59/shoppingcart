package com.shop;


import java.math.BigDecimal;

import com.google.common.base.Function;

public enum ShoppingItem {

    APPLE (new BigDecimal("0.60")),
    ORANGE (new BigDecimal("0.25"));

    private final BigDecimal price;

    private ShoppingItem(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public static Function<String, ShoppingItem> TO_SHOPPING_ITEM = new Function<String, ShoppingItem>() {

        public ShoppingItem apply(String itemName) {

            for (ShoppingItem aShoppingItem : values()) {
                if (aShoppingItem.name().equalsIgnoreCase(itemName)) {
                    return aShoppingItem;
                }
            }
            return null;
        }
    };
   
}
