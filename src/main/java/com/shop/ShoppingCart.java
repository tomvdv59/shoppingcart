package com.shop;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.FluentIterable.of;
import static com.shop.ShoppingItem.APPLE;
import static com.shop.ShoppingItem.ORANGE;
import static com.shop.ShoppingItem.TO_SHOPPING_ITEM;
import static java.math.BigDecimal.ROUND_DOWN;
import static java.math.BigDecimal.ROUND_UP;
import static java.util.Collections.frequency;

import java.math.BigDecimal;
import java.util.List;

public class ShoppingCart {

    public BigDecimal checkout(String... items) {

        List<ShoppingItem> shoppingItems = of(items)
                .transform(TO_SHOPPING_ITEM)
                .filter(notNull())
                .toList();

        int numberOfApples = frequency(shoppingItems, APPLE);
        BigDecimal priceForApples = getPriceForApples(numberOfApples);

        int numberOfOranges = frequency(shoppingItems, ORANGE);
        BigDecimal priceForOranges = getPriceForOranges(numberOfOranges);

        return priceForApples.add(priceForOranges);
    }

    private BigDecimal getPriceForApples(int applesCount) {

        BigDecimal chargedApples = new BigDecimal(applesCount)
                .divide(new BigDecimal(2), ROUND_UP);

        return APPLE.getPrice().multiply(chargedApples);
    }

    private BigDecimal getPriceForOranges(int orangesCount) {

        BigDecimal freeOranges = new BigDecimal(orangesCount)
                .divide(new BigDecimal(3), ROUND_DOWN);

        BigDecimal chargedOranges = new BigDecimal(orangesCount).subtract(freeOranges);

        return ORANGE.getPrice().multiply(chargedOranges);
    }

}
